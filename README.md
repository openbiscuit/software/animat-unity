# Animat-Unity

Animat environment using ROS + Unity.

# Utilisation 

Ce Readme est pour les développeurs. Pour les utilisateurs, il y a une page page web dédiée : [https://openbiscuit.gitlabpages.inria.fr/software/animat-unity/]

## Versions
- 0.6.1 : correction d'un bug lors de la ServerCmd 'reset'
- 0.6 : for Unity2022.3.10f1 => corrections couleur spots; publidh odom; subscribe to consume; upd cube rouge.
- 0.4 : for Unity 2019.4.xx

# Installation pour le Développement

## Unity

Il y a plusieurs `Unity Projets` dans ce dépot.

- `Unity/AnimatRob` créé avec la version 2019.4.xx (LTS) de Unity. (Actuellement, 2019.4.10f1), avec ROS#.

- `Unity/AnimatRobotics` crée avec la version 2022.3.10f1 (LTS) de Unity et ROSTCP-endpoint (UnityRobotics)

- https://unity.com/

### AnimatRobotics

#### depuis le dépot GIT

Quand on clone le projet `AnimatRobotics` depuis le git et qu'on l'ouvre comme un nouveau projet dans Unity, il y a aura de nombreuses erreurs car

- (VERIFIER) il faut ajouter ROSTCP-endpoint de UnityRobotics
- il faut générer les messages utilisé par ROS
- il faut installer YAMLdotNet

#### Préparer Unity pour ROSTCP-Endpoint

**C'est peut être mieux de le faire avant d'importer le projet**

Voir [unitySetup](https://github.com/Unity-Technologies/Unity-Robotics-Hub/blob/main/tutorials/ros_unity_integration/setup.md#-unity-setup).

#### Générer les msg de ROS

Si `ROS` est bien installé dans `Unity`, le menu `Robotics->Generate Ros Messages` permet d'ouvrir une fenêtre pour générer les messages.

- `ROS message path` : là où sont les fichiers de définition des messages. C'est dans un package ROS, et j'ai utilisé `$ANIMAT_ROS/ROS/animat/ros/msg`
- `Built message path`: le répertoire dans lequel sera généré les msg. Moi j'ai mis `$ANIMAT_UNITY/Unity/AnimatRobotics/Assets` pour que `Unity` crée les messages dans `.../Assets/Anima/msg`.

**ATTENTION** bien choisir `ROS2`.

#### Ajouter YAMLdotNet

Dans `Unity`, passer par `Window->Package Manager->My Assets` (parce que j'ai déjà "acheté" YamlDotNet) et cocher `Import`.

#### RUN

On peut lancer le projet `Unity`. ou le compiler.

## ROS

Pour développer côté ROS, il faut que ROS soit installé et disposer du package ROS disponible dans le git [Animat-ROS](git@gitlab.inria.fr:biscuit/software/animat-ros.git).

# Documentation de ce package

Il est constitué de composants (Assets) pour Unity.

## Hiérarchie du dépôt

- `./public` : le site web static du projet
- `./Unity/AnimatRobotics` est un projet Unity+ROSTCP-endpoint.
- `./Unity/AnimatRob` est un projet Unity, permet d'aller modifier l'Animat (forme, physiologie, commandes, etc). => maintenant **OBSOLETE**
- `./Unity/Build` c'est là que je place les versions 'standalone' du simulateur.
- `./Unity/SandBox` ma mémoire de bouts de Scripts, pas nécessaire

## Comment construire l'exécutable 'standalone' du simulateur

Dans Unity: File->Build Settings

**OBSOLETE** Dans PlayerSettings->Player->Other Settings->Api Compatibility Level il faut mettre ".NET 4.x", sinon RosSharp (RosBridgeClient) ne fonctionne pas.

Ensuite, choisir developpment si il y a un problème

Puis "Build"

Attention : dans la fenêtre du jeu, pour le moment, c'est la camera qui est mise en `Display 1` qui est affichée. Par défaut, j'aime bien y mettre la scène vue de haut, et pas la vue du AnimatBot.

Enfin l'archive : `tar czvhf animat_0.x.tgz Animat_0.x` si on a crée le lien `Animat_0.x` vers Build, en y ajoutant `scene_template.yaml`.


## Assets pour construire un environnement

**Pas forcément à jour**

Pour créer une scène Unity, on peut utiliser les objets fournis dans le package `./Unity/AnimatBot.unitypackage`.

### AnimatRob **OBSOLETE**

Il faut y ajouter ROS# et YAMLdotNet.

#### ROS# (pour Unity)

Dans le projet Unity, il faut ajouter ROS#

- depuis le Assets Store de Unity, charger et installer ROS#

Si cela ne fonctionne pas, on peut aussi récupérer ROS# par le biais de son GIT : https://github.com/siemens/ros-sharp

#### YAMLdotNet

Sur l'AssetStore de Unity, `downwload` and `import` YamlDotNet for Unity

Ensuite, dans `Plugins/YamlDotNet/YamlDotNet.asmdef properties`:

il faut spécifier

`"autoReferenced": true`

Des infos là
https://assetstore.unity.com/packages/tools/integration/yamldotnet-for-unity-36292#content
et là
https://github.com/aaubry/YamlDotNet
