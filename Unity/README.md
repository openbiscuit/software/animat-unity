# Animat-v003 : AnimatBot (Server Unity)

Animat environment using ROS + Unity

La version actuelle est la `0.6`, qui utilie UnityRobottics.

# Utilisation 

Voir la page web dédiée :[https://openbiscuit.gitlabpages.inria.fr/software/animat-unity/] 

(Ancienne page [https://biscuit.gitlabpages.inria.fr/animat-v003/])

# Les topics ROS
- `/animat/odom` [pub] pose de l'animat.
- `/animat/server_cmd` [sub] commande pour le simulateur (play/pause/reset).
- `/animat/joint` [pub] articulations de l'animat.
- `/animat/physio` [pub] état de la physiologie interne de l'animat (food/water).
- `/animat/consume_cmd` [sub] action de consomation ("eat", "drink", "none")
- `/animat_vision/compressed` [pub] image (compressed) de la vision de l'animat.
- `/animat/wheel_cmd` [sub] commande de vélocité pour chaque roue.

# Versions
## 0.6.2: can use "reset; 1.0; 2.0; 30.0" pour "reset; posX; posY; orient"
## 0.6 : correction de plusieurs Bugs, améliorations
- les Spots Water/Food sont *vraiment* bleu (0,0,255) et rouge (255,0,0)
- la lumière est une lumière blanche (255,255,255)
- la Cmd "reset" sur `server_cmd` ne créee plus une scène mais m-à-j l'Animat (pose et physio)
- retour de `/animat/odom` et `/animat/consume_cmd`
- *en cours* : tests pour un mode 'headless' (batchmode)
## 0.5 : passage à UnityRobottics, ROS1 et ROS2
## 0.3 : configurer position caméra
- hauteur et orientation bas-haut.
## 0.2 : lumière sans ombre
- directional light -> point light : plus d'ombre sur les murs intérieurs.
## 0.1 : Première version production
