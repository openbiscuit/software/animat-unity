private maxRecursionLevel = 2;
private void ListComponents( GameObject gObj, int level )
{
    if( level == maxRecursionLevel ) return;
    foreach( var comp in gObj.GetComponents<Component>() )
    {
        String indent = ">";//new String(">");
        indent = indent.PadLeft( 2*level+1, '+' );
        //if(component != this) components.Add(component);
        Debug.Log( indent+"C is "+comp.name+" type="+comp.GetType() );
    }
    // recursion : only Transform has childs
    for( int idc = 0; idc < transform.childCount; ++idc )
    {
        ListComponents( transform.GetChild(idc).gameObject, level+1 );
    }
}    
