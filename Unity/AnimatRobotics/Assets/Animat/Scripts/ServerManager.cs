﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * TODO check 'pause' and 'reset_phy' the current simulation.
 *
 *
 */
namespace Animat
{
    /**
     * the UIManager is called when `paused` changes status
     */
    public class ServerManager : MonoBehaviour
    {
        [SerializeField]
        private bool _paused;
        public bool paused {
            get { return _paused; }
            private set {
                _paused = value;
                if (_paused)
                { Time.timeScale = 0.0f; }
                else
                { Time.timeScale = 1.0f; }
            }
        }

        // false only for some test in the Editor.
        public bool build_scene = false;

        // [SerializeField]
        // private AnimatReseter animatReseter = null;
        
        private bool doResetPhy = false;
        private float animatPosX = 0f;
        private float animatPosY = 0f;
        private float animatOrient = 0f; // in degree

        private Scene.ArgsParser argsParser = null;
        private Scene.SceneBuilder sceneBuilder = null;
            
        public void Awake() 
        {
            // initialize needed components
            argsParser = this.gameObject.GetComponent<Scene.ArgsParser>();
            if (build_scene)
            {
                sceneBuilder = this.gameObject.GetComponent<Scene.SceneBuilder>();
            }

            Debug.Log( "__ServerManager.StartGame()" );

            // Parse Arguments
            argsParser.ParseCmdLine();
            if( argsParser.scenePath != null )
            {
                Debug.Log( "  scenePath="+argsParser.scenePath );
                if (build_scene)
                {
                    sceneBuilder.Deserialize( argsParser.scenePath );
                }
            }
            else
            {
                Debug.Log( "  scenePath NOT SET" );
            }
            if (build_scene)
            {
                sceneBuilder.SetEnvironment();
            }
        }
        
        void FixedUpdate()
        {
            if (doResetPhy)
            {
                paused = true;
                //animatReseter.Reset();
                if (build_scene)
                {
                    sceneBuilder.ResetEnvironment( animatPosX, animatPosY, animatOrient );
                    //SceneManager.LoadScene( SceneManager.GetActiveScene().name );
                }
                doResetPhy = false;
            }

            // pause is dne by setting the timeScale to 0
            if( paused )
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
        
        public void UpdateServer( string _cmd )
        {
            if( _cmd == "pause" )
            {
                paused = true;
            }
            else if( _cmd == "play" )
            {
                paused = false;
            }
            //else if( _cmd == "reset" )
            else if( _cmd.StartsWith("reset"))
            {
                // Parse animat x,y and orient from message
                ParseResetMsg(_cmd);
                doResetPhy = true;
            }
            else
            {
                Debug.LogWarning( "ServerManager.UpdateServer : unknown cmd="+_cmd );
            }
        }
        public void PauseServer()
        {
            Debug.Log("__ServerManager PAUSE");
            paused = true;
        }
        public void PlayServer()
        {
            Debug.Log("__ServerManager PLAY");
            paused = false;
        }

        // msg = "reset; xf; yf; orientf"
        private void ParseResetMsg( string msg )
        {
            // split on ';'
            string[] tok = msg.Split(';');
            if ( tok.Length == 4 )
            {
                animatPosX = float.Parse(tok[1], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                animatPosY = float.Parse(tok[2], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                animatOrient = float.Parse(tok[3], System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            }
        }
    }
}
