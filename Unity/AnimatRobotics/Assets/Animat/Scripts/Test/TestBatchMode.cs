using UnityEngine;
using System;

public class TestBatchMode : MonoBehaviour
{
    public GameObject movingCube;
    public Camera batchCamera;

    public bool shouldEnd = true;
    public float endTime = 10.0f;

    public float simuDeltaT = 0.1f;
    public float timeScale = 1.0f;

    bool batchMode = false;

    // Awake is called at creation
    void Awake()
    {
        if (shouldEnd) {
            Debug.LogFormat( "**** Simulator **** will end in {0}s of simulated Time", endTime );
        }
        else {
            Debug.LogFormat( "**** Simulator **** will not end" );
        }

        ParseCmdLine();
        Debug.LogFormat( "  batchmode : {0}", batchMode );

        // Set time parameters
        Time.timeScale = timeScale;
        //Application.targetFrameRate = (int) Math.Floor( 1.0f / simuDeltaT );
        // Sert surtout à savoir quand on quitte : Application.Quit ne fonctionne
        // que sur les Frame/Update
        // TODO Could remove et laisser par défaut, non ?
        Application.targetFrameRate = 10;
        Time.fixedDeltaTime = simuDeltaT * timeScale;
        Debug.LogFormat( "  fixing fixedDeltaTime to {0} ({1})",
                         simuDeltaT * timeScale, simuDeltaT );
    }
    // Start is called before the first frame update
    void Start()
    {

        // Init Camera
        Camera.onPostRender += UpdateImage;
        if (batchMode)
        {
            // In batchMode, we disable Update for the camera.
            batchCamera.enabled = false;
        }
        else
        {
            batchCamera.enabled = true;
        }

        // Print time info on game
        Debug.LogFormat( "**** Time **** scale={0} fixedDT={1}, frameRate={2}",
                         Time.timeScale,
                         Time.fixedDeltaTime,
                         Application.targetFrameRate );


        // Move the cube
        movingCube.GetComponent<Rigidbody>().velocity = new Vector3( 1, 0, 0 );
    }

    // Update is called once per frame
    void Update()
    {
        Debug.LogFormat( "***** FRAME {0,4:N} ******************************************",
                   Time.frameCount );
        Debug.LogFormat( "Update: game={0}\t u_frame={1}\t frame={2}\t uDT={3}\t DT={4}",
                         Time.realtimeSinceStartupAsDouble,
                         Time.unscaledTimeAsDouble,
                         Time.timeAsDouble,
                         Time.unscaledDeltaTime, Time.deltaTime);
        // Print time info on game
        Debug.LogFormat( "**** Time **** scale={0} fixedDT={1}, frameRate={2}",
                         Time.timeScale,
                         Time.fixedDeltaTime,
                         Application.targetFrameRate );
        DebugCube();
    }

    // Called at fixed interval by Physics
    void FixedUpdate()
    {
        Debug.LogFormat( "Fixed: game={0}\t u_fixed={1}\t fixed={2}\t uDF={3}\t DF={4}",
                         Time.realtimeSinceStartupAsDouble,
                         Time.fixedUnscaledTimeAsDouble,
                         Time.fixedTimeAsDouble,
                         Time.fixedUnscaledDeltaTime, Time.fixedDeltaTime);
        DebugCube();

        if (batchMode)
        {
            batchCamera.Render();
        }

        // Stop application when simulated time exceeds the endTime
        if (shouldEnd) {
            if (Time.fixedTimeAsDouble > endTime)
            {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #endif
                // Only taken into account on the next Frame/Update()
                Application.Quit();
            }
        }
    }

    void DebugCube()
    {
        Debug.LogFormat( "   cubeX={0}", movingCube.transform.position.x );
    }

    void UpdateImage(Camera cam)
    {
        if (cam == this.batchCamera)
        {
            TimeLog( "UpdateImage" );
        }
    }

    void TimeLog(string msg)
    {
        Debug.LogFormat( "{0} at {1,10:N6}(uF={2,10:N6})", msg,
                         Time.realtimeSinceStartupAsDouble,
                         Time.fixedUnscaledTimeAsDouble );
    }

    // *************************************************************************
    // ******************************************************************** ARGS
    // *************************************************************************
    void ParseCmdLine()
    {
        string[] args = System.Environment.GetCommandLineArgs ();

        int ida = 0;
        while( ida < args.Length )
        {
            Debug.LogFormat( "{0}: {1}", ida, args[ida] );
            if (args[ida] == "-batchmode" )
            {
                batchMode = true;
            }
            if (args[ida] == "--deltaT")
            {
                ida += 1;
                // float as xx.xxx
                simuDeltaT = float.Parse(args[ida],
                                         System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                Debug.LogFormat( "  simuDeltaT parsed as {0}", simuDeltaT );
            }
            if (args[ida] == "--timeScale")
            {
                ida += 1;
                // float as xx.xxx
                timeScale = float.Parse(args[ida],
                                        System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                Debug.LogFormat( "  scaleTime parsed as {0}", timeScale );
            }
            ida += 1;
        }
    }
}
