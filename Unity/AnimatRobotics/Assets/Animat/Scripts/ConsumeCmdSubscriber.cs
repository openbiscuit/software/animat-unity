using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Animat;
//using RosUtilities.MsgHandling;

namespace Animat
{
    public class ConsumeCmdSubscriber : MonoBehaviour
    {
        [SerializeField]
        private ConsumptionManager consumptionManager = null;

        public string topicName = "animat/consume_cmd";

        protected void Start()
        {
            Debug.Log( "__ConsumeCmd START" );

            ROSConnection.GetOrCreateInstance().
                Subscribe<ConsumeCmdMsg>(topicName,
                ReceiveMessage);
        }
        
        protected void ReceiveMessage( ConsumeCmdMsg msg )
        {
            Debug.Log( "ConsumeCmdMsg consume="+msg.consume );

            if (consumptionManager != null)
            {
                consumptionManager.UpdateConsumption(  msg.consume );
            }
        }
    }
}
