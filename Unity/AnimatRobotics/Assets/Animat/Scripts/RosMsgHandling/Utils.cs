using System;
using UnityEngine;
using RosMessageTypes.Std;
using RosMessageTypes.BuiltinInterfaces;

namespace RosUtilities.MsgHandling
{
    public class Utils
    {
        public const double k_NanosecondsInSecond = 1e9f;

        public static HeaderMsg StampedHeader(string frameID)
        {
            // getTime of last FixedUpdate
            var timeInSeconds = Time.fixedTimeAsDouble;
            var sec = Math.Floor(timeInSeconds);
            var nano = (timeInSeconds - sec) * k_NanosecondsInSecond;

            // Debug.LogFormat( "----- HEADER ------ with t={0}, s={1}, n={2}",
            //                  timeInSeconds, sec, nano );

            return new HeaderMsg(
                new TimeMsg((int)sec, (uint)nano),
                frameID
            );
        }

        public static Vector3 Ros2Unity(Vector3 vector3)
        {
            return new Vector3(-vector3.y, vector3.z, vector3.x);
        }
        public static Vector3 Unity2Ros(Vector3 vector3)
        {
            return new Vector3(vector3.z, -vector3.x, vector3.y);
        }

        public static Vector3 Ros2UnityScale(Vector3 vector3)
        {
            return new Vector3(vector3.y, vector3.z, vector3.x);
        }
        public static Vector3 Unity2RosScale(Vector3 vector3)
        {
            return new Vector3(vector3.z, vector3.x, vector3.y);
        }

        public static Quaternion Ros2Unity(Quaternion quaternion)
        {
            return new Quaternion(quaternion.y, -quaternion.z, -quaternion.x, quaternion.w);
        }
        public static Quaternion Unity2Ros(Quaternion quaternion)
        {
            return new Quaternion(-quaternion.z, quaternion.x, -quaternion.y, quaternion.w);
        }

    }
}
