/*
 * Emulate a Motor driving an ArticulationBody.
 * Implements JoyAxisWriter.
 */

using UnityEngine;

namespace RosUtilities.MsgHandling
{
    [RequireComponent(typeof(ArticulationBody))]
    public class JoyAxisArticulationDriveWriter : JoyAxisWriter
    {
        public float MaxVelocity;

        private ArticulationBody _articulation;
        private float targetVelocity;
        private bool isMessageReceived;

        private void Start()
        {
            _articulation = GetComponent<ArticulationBody>();
        }

        private void Update()
        {
            if (isMessageReceived)
                ProcessMessage();
        }

        private void ProcessMessage()
        {
            Debug.LogFormat("__JoyAxisArticulationDriver Process tgt_vel={0}",
                            targetVelocity);
            // motor by using the Drive
            var drive = _articulation.xDrive;
            drive.targetVelocity = targetVelocity;
            _articulation.xDrive = drive;
            isMessageReceived = false;
        }

        public override void Write(float value)
        {
            targetVelocity = value * MaxVelocity;
            isMessageReceived = true;
        }

        public void ForceValue(float value)
        {
            Write(value);
            ProcessMessage();
        }
    }

}
