using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Animat;
using RosUtilities.MsgHandling;

namespace Animat
{
    public class WheelCmdSubscriber : MonoBehaviour
    {
        [SerializeField]
        private JoyAxisWriter leftWheelWriter=null;
        [SerializeField]
        private JoyAxisWriter rightWheelWriter=null;

        public string topicName = "animat/wheel_cmd";

        protected void Start()
        {
            //base.Start();
            Debug.Log( "__WheelCmd START" );

            ROSConnection.GetOrCreateInstance().
                Subscribe<WheelCmdMsg>(topicName,
                ReceiveMessage);
        }
        
        protected void ReceiveMessage( WheelCmdMsg msg )
        {
            Debug.LogFormat( "WheelCmd left={0} right={1}",
                       msg.vel_left, msg.vel_right );

            if (leftWheelWriter != null)
            {
                leftWheelWriter.Write( msg.vel_left );
            }
            if (rightWheelWriter != null)
            {
                rightWheelWriter.Write( msg.vel_right );
            }
        }
    }
}
