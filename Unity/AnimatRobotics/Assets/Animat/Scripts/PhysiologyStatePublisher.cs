using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Animat;
using RosUtilities.MsgHandling;

// PhysiologyState is published every FixedUpdate
// Attach : animat
namespace Animat
{
    public class PhysiologyStatePublisher : MonoBehaviour
        //UnityPublisher<PhysiologyState>
    {
        [SerializeField]
        private Animat.PhysiologyManager animat = null;
        public string frameId = "Unity";

        private PhysiologyStateMsg msg;

        ROSConnection ros;
        public string topicName = "animat/physio";

        protected void Start()
        {
            Debug.Log("__PhysioPublisher START");
            //base.Start();
            ros = ROSConnection.GetOrCreateInstance();
            ros.RegisterPublisher<PhysiologyStateMsg>(topicName);

            InitializeMessage();
        }
        private void FixedUpdate()
        {
            //Debug.Log("__PhysioPublisher FixedUpdate");
            UpdateMessage();
        }
        private void InitializeMessage()
        {
            msg = new PhysiologyStateMsg();
        }
        private void UpdateMessage()
        {
            // TimeStamp in Header is a *big* question
            // see https://forum.unity.com/threads/ros-unity-clock-publisher-issue.1261922/
            //     https://github.com/Unity-Technologies/Robotics-Nav2-SLAM-Example/tree/main/Nav2SLAMExampleProject/Assets/Scripts

            msg.header = Utils.StampedHeader(frameId);
            msg.food = animat.foodLevel;
            msg.water = animat.waterLevel;

            //Debug.Log("__PhysioPublisher Update sending");
            ros.Publish(topicName, msg);
            //Debug.Log("  send finished");
        }

    }
}
