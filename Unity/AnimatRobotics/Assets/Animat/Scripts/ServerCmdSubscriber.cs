using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Std;

/**
 * forward ServerCmd (as ROS std_msg/string) to a ServerManager
 *
 */
namespace Animat
{
    public class ServerCmdSubscriber : MonoBehaviour
        //UnitySubscriber<RosSharp.RosBridgeClient.MessageTypes.Std.String>
    {
        [SerializeField]
        private ServerManager serverManager = null;

        public string topicName = "animat/server_cmd";

        protected void Start()
        {
            Debug.Log( "__ServerCmd START" );

            ROSConnection.GetOrCreateInstance().
                Subscribe<StringMsg>(topicName,
                ReceiveMessage);

        }
        
        protected void ReceiveMessage( StringMsg msg )
        {
            Debug.Log( "__ServerCmd ="+msg.data );

            if( serverManager != null )
            {
                serverManager.UpdateServer( msg.data );
            }
        }
    }
}
                
