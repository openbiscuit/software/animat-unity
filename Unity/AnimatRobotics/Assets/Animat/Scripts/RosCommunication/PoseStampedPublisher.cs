﻿/*
© Siemens AG, 2017-2018
Author: Dr. Martin Bischoff (martin.bischoff@siemens.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
<http://www.apache.org/licenses/LICENSE-2.0>.
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Added allocation free alternatives
// UoK , 2019, Odysseas Doumas (od79@kent.ac.uk / odydoum@gmail.com)

// Adjustments to UnityRobotis.ROS-TCP-Connector
// Alain Dutech (alain.dutech@loria;fr)

using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Geometry;
using RosUtilities.MsgHandling;

namespace RosUtilities.Communication
{
    public class PoseStampedPublisher : MonoBehaviour
    {
        public Transform PublishedTransform;
        public string FrameId = "Unity";

        private RosMessageTypes.Geometry.PoseStampedMsg message;

        ROSConnection ros;
        public string topicName = "animat/odom";

        protected void Start()
        {
            ros = ROSConnection.GetOrCreateInstance();
            ros.RegisterPublisher<PoseStampedMsg>(topicName);

            InitializeMessage();
        }

        private void FixedUpdate()
        {
            UpdateMessage();
        }

        private void InitializeMessage()
        {
            message = new RosMessageTypes.Geometry.PoseStampedMsg();
            message.header = Utils.StampedHeader(FrameId);
        }

        private void UpdateMessage()
        {
            message.header = Utils.StampedHeader(FrameId); //Update() TODO ?
                                                         //
            GetGeometryPoint(Utils.Unity2Ros(PublishedTransform.position),
                             message.pose.position);
            GetGeometryQuaternion(Utils.Unity2Ros(PublishedTransform.rotation),
                                  message.pose.orientation);

            ros.Publish(topicName, message);
        }

        private static void GetGeometryPoint(Vector3 position,
                                             PointMsg geometryPoint)
        {
            geometryPoint.x = position.x;
            geometryPoint.y = position.y;
            geometryPoint.z = position.z;
        }

        private static void GetGeometryQuaternion(Quaternion quaternion,
                                                  QuaternionMsg geometryQuaternion)
        {
            geometryQuaternion.x = quaternion.x;
            geometryQuaternion.y = quaternion.y;
            geometryQuaternion.z = quaternion.z;
            geometryQuaternion.w = quaternion.w;
        }

    }
}
