using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace Animat
{
    public class UIManager : MonoBehaviour
    {
        public GameObject runningText;
        public GameObject pauseText;

        public Text wheelSpeedtext;
        public ArticulationBody leftWheel;
        public ArticulationBody rightWheel;

        public Text foodLevelText;
        public Text waterLevelText;
        public PhysiologyManager physioManager;

        public ServerManager serverManager;
        
        // should match the one from serverManager
        private bool paused;

        void Start()
        {
            paused = serverManager.paused;
            TogglePaused( paused );
        }
        
        public void Update()
        {
            // Animat velocity
            // We have access to wheel *global* angular velocity (reference to global Frame)
            Vector3 left_spd = leftWheel.angularVelocity;
            Vector3 right_spd = rightWheel.angularVelocity;

            // transform to local angular vector, interested in the Y coordinate
            Vector3 local_leftSpd = leftWheel.transform.InverseTransformVector(left_spd);
            Vector3 local_rightSpd = rightWheel.transform.InverseTransformVector(right_spd);

            // Debug.LogFormat( "UIManager L_angVel={0} R_angVel={1}\n  local L_angVel={2} R_angVel={3}",
            //                  left_spd, right_spd,
            //                  local_leftSpd, local_rightSpd);

            string msgWheel = String.Format( "Wheel spd : {0,6:N1} / {1,6:N1}",
                                            -local_leftSpd.y, local_rightSpd.y );
            wheelSpeedtext.text = msgWheel;

            // Animat food/water Level
            string msgFood = String.Format( "Food level : {0,7:N2}",
                                            physioManager.foodLevel );
            string msgWater = String.Format( "Water level : {0,7:N2}",
                                             physioManager.waterLevel );
            foodLevelText.text = msgFood;
            waterLevelText.text = msgWater;

            // Server Pause/running
            if( paused != serverManager.paused )
            {
                TogglePaused( serverManager.paused );
            }
        }
        
        public void TogglePaused( bool _paused )
        {
            this.paused = _paused;
            Debug.Log( "__UIManager.TogglePaused "+_paused );
            if( this.paused )
            {
                runningText.SetActive( false );
                pauseText.SetActive( true );
            }
            else
            {
                runningText.SetActive( true );
                pauseText.SetActive( false );                
            }
        }
    }
}
