﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * Can 'pause' and 'reset_phy' the current simulation.
 * NEED:
 * - AnimatReseter : often attached to the Animat
 */
namespace Animat
{
    /**
     * the UIManager is called when `paused` changes status
     */
    public class ServerManager : MonoBehaviour
    {
        [SerializeField]
        private bool _paused = true;
        public bool paused {
            get { return _paused; }
            private set { _paused = value; }
        }

        // [SerializeField]
        // private AnimatReseter animatReseter = null;
        
        private int doResetPhy = 0;

        private Scene.ArgsParser argsParser = null;
        private Scene.SceneBuilder sceneBuilder = null;
            
        public void Awake() 
        {
            // initialize needed components
            argsParser = this.gameObject.GetComponent<Scene.ArgsParser>();
            sceneBuilder = this.gameObject.GetComponent<Scene.SceneBuilder>();
            
            Debug.Log( "__ServerManager.StartGame()" );
            paused = true;

            // Parse Arguments
            argsParser.ParseCmdLine();
            if( argsParser.scenePath != null )
            {
                Debug.Log( "  scenePath="+argsParser.scenePath );
                sceneBuilder.Deserialize( argsParser.scenePath );
            }
            else
            {
                Debug.Log( "  scenePath NOT SET" );
            }
            sceneBuilder.SetEnvironment();
            
        }
        
        // Update is called once per frame
        void Update()
        {
            if( doResetPhy > 0 )
            {
                paused = true;
                //animatReseter.Reset();
                SceneManager.LoadScene( SceneManager.GetActiveScene().name );
                doResetPhy = 0;
            }
            if( paused )
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }            
        }
        
        public void UpdateServer( string _cmd )
        {
            if( _cmd == "pause" )
            {
                paused = true;
            }
            else if( _cmd == "play" )
            {
                paused = false;
            }
            else if( _cmd == "reset" )
            {
                doResetPhy = 1;
            }
            else
            {
                Debug.LogWarning( "ServerManager.UpdateServer : unknown cmd="+_cmd );
            }
        }
        public void PauseServer()
        {
            paused = true;
        }
        public void PlayServer()
        {
            paused = false;
        }
    }
}
