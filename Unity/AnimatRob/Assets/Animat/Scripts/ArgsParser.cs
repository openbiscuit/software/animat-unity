using UnityEngine;
using System.IO;
using System;

/**
 * Can detect/react to the following arguments on command line
 * --scenePath <path>
 *
 * if not set, the corresponding Property will be null
 */
namespace Animat.Scene
{
    public class ArgsParser : MonoBehaviour
    {
        public string scenePath { get; private set; }

        // Used when DEBUG
        // void Start()
        // {
        //     Debug.Log( "__ArgParser.Start" );
        //     ParseCmdLine();

        //     if( scenePath != null )
        //     {
        //         Debug.Log( "  scenePath="+scenePath );
        //     }
        //     else
        //     {
        //         Debug.Log( "  scenePath NOT SET" );
        //     }
        // }
        
        public void ParseCmdLine()
        {
            scenePath = null;
        
            string[] args = System.Environment.GetCommandLineArgs ();

            int ida = 0;
            while( ida < args.Length )
            {
                if( args[ida] == "--sceneFile" )
                {
                    scenePath = args[ida+1];
                    ida += 1;
                }
                ida += 1;
            }
        }
    }
}
