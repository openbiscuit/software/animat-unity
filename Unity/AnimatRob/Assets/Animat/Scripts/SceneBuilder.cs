using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System.IO;
using System;
using RosSharp.RosBridgeClient;

/**
 * A Scene is described using Parameters.
 * Can be Serialized to a string, or into a File
 * Can be Deserialized from a File
 *
 * SceneBuilder.defautSceneParameters holds a default configuration.
 *
 * Uses YAMLdotNet for serialization/deserialization
 * see: https://github.com/aaubry/YamlDotNet
 */
namespace Animat.Scene
{
    // *************************************************************************
    // ************************************************************ SceneBuilder
    public class SceneBuilder : MonoBehaviour
    {
        public GameObject foodSpot = null;
        public GameObject waterSpot = null;
        public GameObject animat = null;
        public PhysiologyManager physiologyManager = null;
        public ConsumptionManager consumptionManager = null;
        public GameObject scene = null;
        
        private Parameters sceneParameters = defaultSceneParameters;

        // Mainly used for DEBUG
        // void Start()
        // {
        //     //sceneParameters = defaultSceneParameters;
        //     Debug.Log( "__SceneBuilder Serialize to scene_template.yaml" );
        //     Serialize( "scene_template.yaml", defaultSceneParameters );
        //     //SetEnvironment();
        // }

        public void SetEnvironment()
        {
            var param = sceneParameters;
            
            // TODO: Environment
            Environment env = param.environment;
            // A default plane has size 5x5
            Vector3 scalePlane = new Vector3( env.sizeX / 5.0f,
                                              1f,
                                              env.sizeY / 5.0f );
            scene.transform.localScale = scalePlane;
            // Walls are child of scene, so position will be changed (even if
            // the width of walls will also be scaled).
            
            // Spots
            foreach( var pos in param.foodPos  )
            {
                Instantiate( foodSpot, pos.ToVector3(), Quaternion.identity );
            }
            foreach( var pos in param.waterPos  )
            {
                Instantiate( waterSpot, pos.ToVector3(), Quaternion.identity );
            }

            // Set Robot Pose 
            // Search the current components to find body_link Transform
            foreach( var comp in animat.GetComponentsInChildren<Transform>() )
            {
                if( comp.name == "body_link" )
                {
                    Debug.Log( "Set Body at "+ param.animatPos.ToVector3());
                    Transform bodyLink = comp;
                    bodyLink.SetPositionAndRotation( param.animatPos.ToVector3(),
                                                     param.AnimatRot() );
                }
                else if( comp.name == "right_wheel_link" ||
                         comp.name == "left_wheel_link" )
                {
                    
                    JoyAxisJointMotorWriter joyWriter = comp.gameObject.GetComponent<JoyAxisJointMotorWriter>();
                    joyWriter.MaxVelocity = param.animatMaxVelocity;
                }
                // TODO: start velocity is 0,0
                else if( comp.name == "camera_pos" )
                {
                    // Needed to add a new child of camera_link as a fixedJoint
                    // cannot be Translated or Rotated. Even in Awake()
                    Vector3 linkPos = new Vector3( 0f, param.cameraHeight, 0f );
                    Vector3 eulerRot = new Vector3( param.cameraPitchDeg, 0f, 0f);
                    
                    comp.position = linkPos;
                    comp.Rotate( eulerRot );
                }
            }

            // Set Physio
            physiologyManager.foodMax = param.physiology.foodMax;
            physiologyManager.foodStart = param.physiology.foodStart;
            physiologyManager.foodConsumptionRate = param.physiology.foodRate;
            physiologyManager.waterMax = param.physiology.waterMax;
            physiologyManager.waterStart = param.physiology.waterStart;
            physiologyManager.waterConsumptionRate = param.physiology.waterRate;

            consumptionManager.foodIn = param.physiology.foodIntake;
            consumptionManager.waterIn = param.physiology.waterIntake;            
        }
        public void Serialize( string path, Parameters param )
        {
            // open and will erase (false)
            StreamWriter writer = new StreamWriter( path, false );
            writer.WriteLine( Serialize() );
            writer.Close();
        }
        /**
         * "manually" add comments in front of main fields
         */
        public string Serialize()
        {
            var serializer = new SerializerBuilder().Build();
            var yaml = serializer.Serialize( sceneParameters );

            // insert help about environment
            int index = yaml.IndexOf( "environment:" );
            yaml = yaml.Insert( index, helpEnvMsg );

            // same for animat, food, water...
            index = yaml.IndexOf( "animatPos:" );
            yaml = yaml.Insert( index, helpAnimatMsg );

            index = yaml.IndexOf( "cameraHeight:" );
            yaml = yaml.Insert( index, helpCameraMsg );
            
            index = yaml.IndexOf( "physiology:" );
            yaml = yaml.Insert( index, helpPhysioMsg );
            
            index = yaml.IndexOf( "foodPos:" );
            yaml = yaml.Insert( index, helpFoodMsg );

            index = yaml.IndexOf( "waterPos:" );
            yaml = yaml.Insert( index, helpWaterMsg );
            
            return yaml;
        }
        public void Deserialize( string path )
        {
            StreamReader reader = new StreamReader(path);
            string yaml = reader.ReadToEnd();
            reader.Close();

            var deserializer = new DeserializerBuilder()
                //.WithNamingConvention(new CamelCaseNamingConvention())
                .Build();

            sceneParameters = deserializer.Deserialize<Parameters>(yaml);
        }

        /**
         * Default Scene Configuration
         */
        static public Parameters defaultSceneParameters = new Parameters {
            environment = new Environment {
                sizeX = 10,
                sizeY = 10,
                withWall = true
            },

            animatPos = new Vec3{
                coords = new float[] {0f, 0.15f, 0f}
            },
            animatRotDeg = 10f,
            animatMaxVelocity = 200f,

            cameraHeight = 1.0f,
            cameraPitchDeg = 30f,
            
            physiology = new Physiology {
                foodMax = 100f,
                foodStart =   100f,
                foodRate =   0.05f,
                foodIntake =  100f,

                waterMax = 100f,
                waterStart =  100f,
                waterRate =  0.05f,
                waterIntake = 100f
            },
            
            foodPos = new Vec3[] {
                new Vec3{
                    coords = new float[] {5f, 0.001f, 5f }
                }
            },
            waterPos = new Vec3[] {
                new Vec3{
                    coords = new float[] {-5f, 0.001f, -5f }
                }
            }
        };

        /**
         * Messages that are inserted when Serialize()
         */
        private const string helpEnvMsg = @"## YAML file to describe a Scene
## Environment : size of plane and presence of exterior walls
## WARN : not checking position of animat, food, water...
## TODO : wall always present
";
        private const string helpAnimatMsg = @"
## Position and rotation (degree) of the Animat
## Y is UP, and should be 0.15
## The MaxVelocity is for the wheels
";
        private const string helpCameraMsg = @"
## Camera: specify height and pitch (positive = look down)
";
        private const string helpPhysioMsg = @"
## Physiology constants : xxxStart at start, xxxRate consumption, xxxIntake when ingest
";
        private const string helpFoodMsg = @"
## Positions of every FoodSpot
## Y is UP, and should be 0.001 (or less, but > 0)
";
        private const string helpWaterMsg = @"
## Positions of every WaterSpot
## Y is UP, and should be 0.001 (or less, but > 0)
";        
    }

    // *************************************************************************
    // ************************************************************** Parameters
    /**
     * Class to hold the parameters to describe a scene.
     * YAMLdotNet can only deal with Properties
     */
    public class Parameters
    {
        public Environment environment { get; set; }

        public Vec3 animatPos { get; set; }
        public float animatRotDeg { get; set; }        
        public float animatMaxVelocity { get; set; }

        public float cameraHeight { get; set; }
        public float cameraPitchDeg { get; set; }

        public Physiology physiology { get; set; }
        
        public Vec3[] foodPos { get; set; }
        public Vec3[] waterPos { get; set; }

        public Quaternion AnimatRot()
        {
            return Quaternion.AngleAxis( animatRotDeg, Vector3.up );
        }
        public Quaternion CameraRot()
        {
            return Quaternion.AngleAxis( cameraPitchDeg, Vector3.right );
        }
    }
    public class Environment
    {
        public float sizeX { get; set; }
        public float sizeY { get; set; }
        public bool withWall { get; set; }
    }
    public class Physiology
    {
        public float foodMax { get; set; }
        public float foodStart { get; set; }
        public float foodRate { get; set; }
        public float foodIntake { get; set; }

        public float waterMax { get; set; }
        public float waterStart { get; set; }
        public float waterRate { get; set; }
        public float waterIntake { get; set; }
    }
    public class Vec3
    {
        public float[] coords { get; set; }
        public Vector3 ToVector3()
        {
            return new Vector3( coords[0], coords[1], coords[2] );
        }
    }
}
