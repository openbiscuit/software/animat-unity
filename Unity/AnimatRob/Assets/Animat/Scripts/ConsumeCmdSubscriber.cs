using UnityEngine;

namespace RosSharp.RosBridgeClient
{
    public class ConsumeCmdSubscriber : Subscriber<Messages.ConsumeCmd>
    {
        [SerializeField]
        private Animat.ConsumptionManager consumptionManager = null;
        
        protected override void Start()
        {
            base.Start();
            Debug.Log( "__ConsumeCmd START" );
        }
        
        protected override void ReceiveMessage( Messages.ConsumeCmd msg )
        {
            Debug.Log( "ConsumeCmd MSG" );
            Debug.Log( "ConsumeCmd consume="+msg.consume );

            if (consumptionManager != null)
            {
                consumptionManager.UpdateConsumption(  msg.consume );
            }
        }
    }
}
