using UnityEngine;

namespace RosSharp.RosBridgeClient
{
    public class WheelCmdSubscriber : Subscriber<Messages.WheelCmd>
    {
        [SerializeField]
        private JoyAxisWriter leftWheelWriter=null;
        [SerializeField]
        private JoyAxisWriter rightWheelWriter=null;
        
        protected override void Start()
        {
            base.Start();
            Debug.Log( "__WheelCmd START" );
        }
        
        protected override void ReceiveMessage( Messages.WheelCmd msg )
        {
            // Debug.LogFormat( "WheelCmd left={0} right={1}",
            //            msg.vel_left, msg.vel_right );

            if (leftWheelWriter != null)
            {
                leftWheelWriter.Write( msg.vel_left );
            }
            if (rightWheelWriter != null)
            {
                rightWheelWriter.Write( msg.vel_right );
            }
        }
    }
}
