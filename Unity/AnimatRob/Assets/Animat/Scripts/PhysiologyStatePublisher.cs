using UnityEngine;

// PhysiologyState is published every FixedUpdate
// Attach : animat
namespace RosSharp.RosBridgeClient
{
    public class PhysiologyStatePublisher : Publisher<Messages.PhysiologyState>
    {
        [SerializeField]
        private Animat.PhysiologyManager animat = null;
        public string FrameId = "Unity";

        private Messages.PhysiologyState msg;

        protected override void Start()
        {
            base.Start();
            InitializeMessage();
        }
        private void FixedUpdate()
        {
            UpdateMessage();
        }
        private void InitializeMessage()
        {
            msg = new Messages.PhysiologyState
            {
                header = new Messages.Standard.Header { frame_id = FrameId },
                food = new float(),
                water = new float()
            };
        }
        private void UpdateMessage()
        {
            msg.header.Update();
            msg.food = animat.foodLevel;
            msg.water = animat.waterLevel;

            Publish(msg);
        }
    }
}
