using UnityEngine;

// Give a GameObject the capacity to probe down, at a maximum distance,
// to detect Food/Water Spot and then give Input to a PhysiologyManager.
//
// When the UpdateConsume is set (to "eat" or "drink"), the Animat will try
// the action every FixedUpdate until fullfilled.
//
// Attach to a GameObject (to use transform)

// TODO Possible improvements:
// - have the food/water Input depend on the particular Spot
// - have the manager Instantiate the Food/WaterSPot (??)
namespace Animat
{
    public class ConsumptionManager : MonoBehaviour
    {
        [SerializeField]
        private PhysiologyManager physio = null;
        [SerializeField]
        public float foodIn = 100.0f;
        [SerializeField]
        public float waterIn = 100.0f;
        [SerializeField]
        private float maxRayLength = 1.0f;

        private bool doUpdate = false; // looking to "eat" or "drink" ?
        private string consume = "none";

        void FixedUpdate()
        {
            if (doUpdate)
            {
                // Every GameObject has a transform
                Ray ray = new Ray( transform.position, Vector3.down );
                RaycastHit hit;

                Debug.DrawLine (transform.position, transform.position + Vector3.down * maxRayLength, Color.red);

                if (Physics.Raycast( ray, out hit, maxRayLength))
                {
                    if (hit.collider.CompareTag( "Food" ) && consume == "eat")
                    {
                        physio.UpdateInput( foodIn, 0.0f );
                        Debug.Log( "Animat can EAT" );
                    }
                    if (hit.collider.CompareTag( "Water" ) && consume == "drink")
                    {
                        physio.UpdateInput( 0.0f, waterIn );
                        Debug.Log( "Animat can DRINK" );
                    }
                }

                doUpdate = false;
            }
        }
        public void UpdateConsumption( string _consume )
        {
            consume = _consume;
            doUpdate = true;
            Debug.Log( "Animat in consume="+consume );
        }
    }
}
                    
                
