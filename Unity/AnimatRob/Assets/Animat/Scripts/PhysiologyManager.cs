using UnityEngine;

namespace Animat
{
    public class PhysiologyManager : MonoBehaviour
    {
        // using [SerializeField] allow it to be set in Inspector
        // while staying private. Seems a good practice.
        [SerializeField]
        public float foodMax = 100.0f;
        [SerializeField]
        public float foodStart = 0.0f;
        [SerializeField]
        public float foodConsumptionRate = 0.1f;
        [SerializeField]
        public float waterMax = 100.0f;
        [SerializeField]
        public float waterStart = 0.0f;
        [SerializeField]
        public float waterConsumptionRate = 0.1f;

        public float foodLevel
        { get; private set;}
        public float waterLevel
        { get; private set;}

        private bool newPhysioInput = false;
        private float foodIn = 0.0f;
        private float waterIn = 0.0f;

        // Initialize physiology variables
        void Start()
        {
            foodLevel = foodStart;
            waterLevel = waterStart;
        }

        // Physical model need to be updated at fixed time intervale
        // independantly of frame computation.
        void FixedUpdate()
        {
            float foodDelta = - foodConsumptionRate * foodLevel;
            foodLevel += foodIn + Time.fixedDeltaTime * foodDelta;
            if (foodLevel > foodMax) foodLevel = foodMax;

            float waterDelta = - waterConsumptionRate * waterLevel;
            waterLevel += waterIn + Time.fixedDeltaTime * waterDelta;
            if (waterLevel > waterMax) waterLevel = waterMax;
            
            if (newPhysioInput)
            {
                newPhysioInput = false;
                foodIn = 0.0f;
                waterIn = 0.0f;
            }

            // TODO Publish
        }

        public void UpdateInput( float _foodInput, float _waterInput )
        {
            foodIn = _foodInput;
            waterIn = _waterInput;
            newPhysioInput = true;
        }
    }
}
