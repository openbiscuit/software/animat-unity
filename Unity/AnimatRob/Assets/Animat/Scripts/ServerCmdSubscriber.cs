using UnityEngine;

/**
 * forward ServerCmd (as ROS std_msg/string) to a ServerManager
 *
 */
namespace RosSharp.RosBridgeClient
{
    public class ServerCmdSubscriber : Subscriber<Messages.Standard.String>
    {
        [SerializeField]
        private Animat.ServerManager serverManager = null;
    
        protected override void Start()
        {
            base.Start();
            Debug.Log( "__ServerCmd START" );
        }
        
        protected override void ReceiveMessage( Messages.Standard.String msg )
        {
            Debug.Log( "__ServerCmd ="+msg.data );

            if( serverManager != null )
            {
                serverManager.UpdateServer( msg.data );
            }
        }
    }
}
                
