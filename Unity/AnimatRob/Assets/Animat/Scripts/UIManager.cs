using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace Animat
{
    public class UIManager : MonoBehaviour
    {
        public GameObject runningText;
        public GameObject pauseText;

        public Text wheelSpeedtext;
        public HingeJoint leftWheel;
        public HingeJoint rightWheel;

        public Text foodLevelText;
        public Text waterLevelText;
        public PhysiologyManager physioManager;

        public ServerManager serverManager;
        
        // should match the one from serverManager
        private bool paused = true;

        void Start()
        {
            TogglePaused( paused );
        }
        
        public void Update()
        {
            // Animat velocity
            float left_spd = leftWheel.velocity;
            float right_spd = rightWheel.velocity;

            string msgWheel = String.Format( "Wheel spd : {0,6:N1} / {1,6:N1}",
                                             left_spd, right_spd );
            wheelSpeedtext.text = msgWheel;

            // Animat food/water Level
            string msgFood = String.Format( "Food level : {0,7:N2}",
                                            physioManager.foodLevel );
            string msgWater = String.Format( "Water level : {0,7:N2}",
                                             physioManager.waterLevel );
            foodLevelText.text = msgFood;
            waterLevelText.text = msgWater;

            // Server Pause/running
            if( paused != serverManager.paused )
            {
                TogglePaused( serverManager.paused );
            }
        }
        
        public void TogglePaused( bool _paused )
        {
            this.paused = _paused;
            Debug.Log( "__UIManager.TogglePaused "+_paused );
            if( this.paused )
            {
                runningText.SetActive( false );
                pauseText.SetActive( true );
            }
            else
            {
                runningText.SetActive( true );
                pauseText.SetActive( false );                
            }
        }
    }
}
