using UnityEngine;
using System.Collections;
using UnityEditor;
using Animat.Scene;

/**
 * Adds a Button in the Inspector for `SceneBuilder` that offer 
 * saving a default YAML template for a scene.
 */ 
[CustomEditor(typeof(Animat.Scene.SceneBuilder))]
public class CustomSceneBuilder : UnityEditor.Editor
{
    private SceneBuilder sceneBuilder;
    
    public override void OnInspectorGUI()
    {
        // get the SceneBuilder Component
        sceneBuilder = (SceneBuilder) target;
        
        DrawDefaultInspector();

        if( GUILayout.Button( "Save scene_template.yaml") )
        {
            sceneBuilder.Serialize( "scene_template.yaml",
                                    SceneBuilder.defaultSceneParameters );
        }

        if( GUILayout.Button( "Load scene_template.yaml") )
        {
            sceneBuilder.Deserialize( "scene_template.yaml" );
            sceneBuilder.SetEnvironment();
        }
    }
}
