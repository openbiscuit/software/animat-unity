/*
  This message class is generated automatically with 'SimpleMessageGenerator' of ROS#
*/ 

using Newtonsoft.Json;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Messages.Geometry;
using RosSharp.RosBridgeClient.Messages.Navigation;
using RosSharp.RosBridgeClient.Messages.Sensor;
using RosSharp.RosBridgeClient.Messages.Standard;
using RosSharp.RosBridgeClient.Messages.Actionlib;

namespace RosSharp.RosBridgeClient.Messages
{
    public class WheelCmd : Message
    {
        [JsonIgnore]
        public const string RosMessageName = "animat/WheelCmd";

        public Header header;
        public float vel_left;
        public float vel_right;

        public WheelCmd()
        {
            header = new Header();
            //vel_left = new float();
            //vel_right = new float();
        }
    }
}
