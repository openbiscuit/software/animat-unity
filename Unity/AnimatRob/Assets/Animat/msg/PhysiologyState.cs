/*
  This message class is generated automatically with 'SimpleMessageGenerator' of ROS#
*/ 

using Newtonsoft.Json;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Messages.Geometry;
using RosSharp.RosBridgeClient.Messages.Navigation;
using RosSharp.RosBridgeClient.Messages.Sensor;
using RosSharp.RosBridgeClient.Messages.Standard;
using RosSharp.RosBridgeClient.Messages.Actionlib;

namespace RosSharp.RosBridgeClient.Messages
{
    public class PhysiologyState : Message
    {
        [JsonIgnore]
        public const string RosMessageName = "animat/PhysiologyState";

        public Header header;
        public float food;
        public float water;

        public PhysiologyState()
        {
            header = new Header();
            //food = new float();
            //water = new float();
        }
    }
}

