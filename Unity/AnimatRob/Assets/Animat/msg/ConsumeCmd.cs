/*
  This message class is generated automatically with 'SimpleMessageGenerator' of ROS#
*/ 

using Newtonsoft.Json;
using RosSharp.RosBridgeClient;
using RosSharp.RosBridgeClient.Messages.Geometry;
using RosSharp.RosBridgeClient.Messages.Navigation;
using RosSharp.RosBridgeClient.Messages.Sensor;
using RosSharp.RosBridgeClient.Messages.Standard;
using RosSharp.RosBridgeClient.Messages.Actionlib;

namespace RosSharp.RosBridgeClient.Messages
{
    public class ConsumeCmd : Message
    {
        [JsonIgnore]
        public const string RosMessageName = "animat/ConsumeCmd";

        public Header header;
        public string consume;

        public ConsumeCmd()
        {
            header = new Header();
        }
    }
}
